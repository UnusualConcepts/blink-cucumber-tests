package ru.uc.blink.cucumber;


import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.ru.Когда;
import cucumber.api.java.ru.Тогда;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import ru.uc.blink.pages.MainPage;

import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class HelloStepDef {
    private static WebDriver driver;
    private MainPage mainPage;

    @Before
    public static void setUp() {
        ChromeOptions chromeOptions = new ChromeOptions();

        chromeOptions.addArguments("--headless");

        driver = new ChromeDriver(chromeOptions);
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        String appAddress = String.format("http://%s:%s/", System.getenv("APP_HOST"), System.getenv("APP_PORT"));
        System.out.printf("APP ADDRESS: %S\n", appAddress);
        driver.get(appAddress);
    }

    @After
    public static void tearDown() {
        driver.close();
    }

    @Когда("я открываю главную страницу")
    public void iOpenMainPage() {
        mainPage = new MainPage(driver);
        assertTrue(mainPage.isInitialized());
    }

    @Тогда("я вижу текст {string}")
    public void i_see_text(String message) {
        assertEquals(mainPage.message.getText(), message);
    }


}
