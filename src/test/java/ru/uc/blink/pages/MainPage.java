package ru.uc.blink.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage extends PageObject {
//    @FindBy(xpath = "//*[@id=\"messsage\"]")
    @FindBy(id="message")
    public WebElement message;

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public boolean isInitialized() {
        return message.isDisplayed();
    }
}

